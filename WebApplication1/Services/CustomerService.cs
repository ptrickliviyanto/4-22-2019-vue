﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Entities;
using WebApplication1.ViewModels;

namespace WebApplication1.Services
{
    public class CustomerService
    {
        private readonly TrainingDbContext _Db;

        public CustomerService(TrainingDbContext trainingDbContext)
        {
            this._Db = trainingDbContext;
        }

        public async Task<List<CustomerViewModel>> GetAllCustomerAsync()
        {
            var allData = await this._Db.Customer.AsNoTracking().Select(Q => new CustomerViewModel
            {
                customerId = Q.CustomerId,
                customerName = Q.CustomerName,
                customerRole = Q.CustomerRole,
            }).ToListAsync();

            return allData;
        } 

        public async Task<bool> InsertCustomerAsync(CustomerViewModel parameterCustomer)
        {
            var insert = new Customer
            {
                CustomerName = parameterCustomer.customerName,
                CustomerRole = parameterCustomer.customerRole,
            };
            this._Db.Customer.Add(insert);
            await this._Db.SaveChangesAsync();
            return true;
        }
    }
}
