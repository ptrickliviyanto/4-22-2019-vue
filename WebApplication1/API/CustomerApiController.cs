﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Services;
using WebApplication1.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1.API
{
    [Route("api/v1/customer")]
    [ApiController]
    public class CustomerApiController : ControllerBase
    {
        private readonly CustomerService _CustomerMan;

        public CustomerApiController(CustomerService customerService)
        {
            this._CustomerMan = customerService;
        }
        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> GetAllCustomer()
        {
            var allData = await this._CustomerMan.GetAllCustomerAsync();
            if(allData.Count == 0)
            {
                return BadRequest("TESTEST");
            }
            return Ok(allData);
        }
        // POST api/<controller>
        [HttpPost("InsertCustomer")]
        public async Task<IActionResult> InsertCustomer([FromBody]CustomerViewModel parameterCustomer)
        {
            if(ModelState.IsValid == false)
            {
                return BadRequest("Model is not valid!");
            }
            var insert = await this._CustomerMan.InsertCustomerAsync(parameterCustomer);

            if(insert == false)
            {
                return BadRequest("Failed insert customer!");
            }
            return Ok("Success to insert db");
        }
    }
}
