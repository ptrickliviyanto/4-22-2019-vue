﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.ViewModels
{
    public class CustomerViewModel
    {
        public int customerId { get; set; }
        public string customerName { get; set; }
        public string customerRole { get; set; }
    }
}
