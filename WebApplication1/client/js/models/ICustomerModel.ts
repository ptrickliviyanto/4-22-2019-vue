﻿//import Axios from 'axios';
export interface ICustomerModel {
    customerId?: number;
    customerName?: string;
    customerRole?: string;
}