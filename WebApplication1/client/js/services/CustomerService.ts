﻿import Axios from 'axios';
import { ICustomerModel } from '../models/ICustomerModel';

export class CustomerService {
    allCustomer: ICustomerModel[] = [];

    async GetAllCustomer() {
        let response = await Axios.get<ICustomerModel[]>('/api/v1/customer');
        this.allCustomer = response.data;
    }

    async InsertCustomer(value: ICustomerModel) {
        try {
            let response = await Axios.post<boolean>('/api/v1/customer/InsertCustomer', value);
            console.log(response.data);
        } catch (error) {
            console.log(error.response.data);
        }
    }
}
export let customerServiceSingleton = new CustomerService();
